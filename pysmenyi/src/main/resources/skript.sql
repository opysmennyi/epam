-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Moja_konfetka
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Moja_konfetka
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Moja_konfetka` ;
USE `Moja_konfetka` ;

-- -----------------------------------------------------
-- Table `Moja_konfetka`.`agency`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`agency` (
  `id_agency` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `payment_terms` VARCHAR(45) NULL,
  `markup` INT NULL,
  `agency_services_price` INT NULL,
  `agreemen_termination` VARCHAR(45) NULL,
  PRIMARY KEY (`id_agency`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`personal_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`personal_info` (
  `passport_id` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `age` VARCHAR(45) NULL,
  `family_status` VARCHAR(45) NULL,
  PRIMARY KEY (`passport_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`agent`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`agent` (
  `id_agent` INT NOT NULL,
  `personal_info_passport_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_agent`, `personal_info_passport_id`),
  INDEX `fk_agent_personal_info1_idx` (`personal_info_passport_id` ASC) VISIBLE,
  CONSTRAINT `fk_agent_personal_info1`
    FOREIGN KEY (`personal_info_passport_id`)
    REFERENCES `Moja_konfetka`.`personal_info` (`passport_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`client` (
  `id` INT NOT NULL,
  `agent_id_agent` INT NOT NULL,
  `personal_info_personal_id` VARCHAR(45) NOT NULL,
  `personal_info_passport_id` VARCHAR(45) NOT NULL,
  INDEX `fk_client_personal_data_idx` (`id` ASC) VISIBLE,
  PRIMARY KEY (`agent_id_agent`, `personal_info_personal_id`, `personal_info_passport_id`),
  INDEX `fk_client_personal_info1_idx` (`personal_info_passport_id` ASC) VISIBLE,
  CONSTRAINT `fk_client_personal_data`
    FOREIGN KEY (`id`)
    REFERENCES `Moja_konfetka`.`personal_data` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_client_agent1`
    FOREIGN KEY (`agent_id_agent`)
    REFERENCES `Moja_konfetka`.`agent` (`id_agent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_client_personal_info1`
    FOREIGN KEY (`personal_info_passport_id`)
    REFERENCES `Moja_konfetka`.`personal_info` (`passport_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`landlord`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`landlord` (
  `id_landor` INT NOT NULL,
  `personal_info_passport_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_landor`, `personal_info_passport_id`),
  INDEX `fk_landlord_personal_info1_idx` (`personal_info_passport_id` ASC) VISIBLE,
  CONSTRAINT `fk_landlord_personal_info1`
    FOREIGN KEY (`personal_info_passport_id`)
    REFERENCES `Moja_konfetka`.`personal_info` (`passport_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`regestry_number`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`regestry_number` (
  `regestry_number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`regestry_number`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`place`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`place` (
  `id_place` INT NOT NULL,
  `square` INT NULL,
  `price` INT NULL,
  `location` VARCHAR(45) NULL,
  `type` VARCHAR(45) NULL,
  `landlord_id_landor` INT NOT NULL,
  `regestry_number_regestry_number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_place`),
  INDEX `fk_place_landlord1_idx` (`landlord_id_landor` ASC) VISIBLE,
  INDEX `fk_place_regestry_number1_idx` (`regestry_number_regestry_number` ASC) VISIBLE,
  CONSTRAINT `fk_place_landlord1`
    FOREIGN KEY (`landlord_id_landor`)
    REFERENCES `Moja_konfetka`.`landlord` (`id_landor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_place_regestry_number1`
    FOREIGN KEY (`regestry_number_regestry_number`)
    REFERENCES `Moja_konfetka`.`regestry_number` (`regestry_number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`agent_has_agency`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`agent_has_agency` (
  `agent_id_agent` INT NOT NULL,
  `agency_id_agency` INT NOT NULL,
  PRIMARY KEY (`agent_id_agent`, `agency_id_agency`),
  INDEX `fk_agent_has_agency_agency1_idx` (`agency_id_agency` ASC) VISIBLE,
  INDEX `fk_agent_has_agency_agent1_idx` (`agent_id_agent` ASC) VISIBLE,
  CONSTRAINT `fk_agent_has_agency_agent1`
    FOREIGN KEY (`agent_id_agent`)
    REFERENCES `Moja_konfetka`.`agent` (`id_agent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_agent_has_agency_agency1`
    FOREIGN KEY (`agency_id_agency`)
    REFERENCES `Moja_konfetka`.`agency` (`id_agency`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`agent_has_landlord`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`agent_has_landlord` (
  `agent_id_agent` INT NOT NULL,
  `landlord_id_landor` INT NOT NULL,
  PRIMARY KEY (`agent_id_agent`, `landlord_id_landor`),
  INDEX `fk_agent_has_landlord_landlord1_idx` (`landlord_id_landor` ASC) VISIBLE,
  INDEX `fk_agent_has_landlord_agent1_idx` (`agent_id_agent` ASC) VISIBLE,
  CONSTRAINT `fk_agent_has_landlord_agent1`
    FOREIGN KEY (`agent_id_agent`)
    REFERENCES `Moja_konfetka`.`agent` (`id_agent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_agent_has_landlord_landlord1`
    FOREIGN KEY (`landlord_id_landor`)
    REFERENCES `Moja_konfetka`.`landlord` (`id_landor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `Moja_konfetka` ;

-- -----------------------------------------------------
-- Placeholder table for view `Moja_konfetka`.`view1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`view1` (`id` INT);

-- -----------------------------------------------------
-- View `Moja_konfetka`.`view1`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Moja_konfetka`.`view1`;
USE `Moja_konfetka`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
