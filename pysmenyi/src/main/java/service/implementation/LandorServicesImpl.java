package service.implementation;

import dao.implementation.LandorDaoImpl;
import model.entitydata.Landlord;
import service.inrterfaces.LandlordServices;
import java.sql.SQLException;
import java.util.List;

public class LandorServicesImpl implements LandlordServices {

    @Override
    public List<Landlord> findAll() throws SQLException {
        return new LandorDaoImpl().findAll();
    }

    @Override
    public Landlord findById(Integer id_landlord) throws SQLException {
        return new LandorDaoImpl().findById(id_landlord);
    }

    @Override
    public int create(Landlord landlord) throws SQLException {
        return new LandorDaoImpl().create(landlord);
    }

    @Override
    public int update(Landlord landlord) throws SQLException {
        return new LandorDaoImpl().update(landlord);
    }

    @Override
    public int delete(Landlord id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_landlord) throws SQLException {
        return new LandorDaoImpl().delete(id_landlord);
    }
}
