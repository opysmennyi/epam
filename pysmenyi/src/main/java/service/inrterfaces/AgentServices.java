package service.inrterfaces;

import model.entitydata.Agent;

import java.sql.SQLException;

public interface AgentServices extends GeneralServices<Agent, Integer> {
    int delete(Integer id_agent) throws SQLException;
}
