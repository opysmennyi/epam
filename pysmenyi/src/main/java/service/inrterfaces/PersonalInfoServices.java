package service.inrterfaces;

import model.entitydata.Personal_info;

import java.sql.SQLException;

public interface PersonalInfoServices extends GeneralServices<Personal_info, Integer> {
    int delete(Integer passport_id) throws SQLException;
}
