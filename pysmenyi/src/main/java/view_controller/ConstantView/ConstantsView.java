package view_controller.ConstantView;

import java.util.Scanner;

public class ConstantsView {

    public static final String CREATED = "There are created rows \n";
    public static final String DELETED = "There are deleted rows \n";
    public static final Scanner INPUT = new Scanner(System.in);

    public static final String CREATE = "Create ";
    public static final String UPDATE = "Update ";
    public static final String DELETE = "Delete ";
    public static final String SELECT = "Select ";
    public static final String FINDBYID = "Find by ID ";

    public static final String AGENCY = "Agency";
    public static final String AGENT = "Agent";
    public static final String AHA = "Agent has agency";
    public static final String AHL = "Agent has landlord";
    public static final String CLIENT = "Client";
    public static final String LANDLORD = "Landlord";
    public static final String PERSONALINFO = "Personal Info";
    public static final String PLACE = "Place";
    public static final String REGESTRYNUMBER = "Regestry number";

}
