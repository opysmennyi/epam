package persistant.persistant_implementation;

import model.properties.Property;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import persistant.constant_connection.ConstantConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class ConnectionManager {

    private static Connection CONNECTION = null;
    public static Logger LOG = LogManager.getLogger(ConnectionManager.class);
    private static final String URL = Property.getProperty(ConstantConnection.URL_KEY);
    private static final String USER = Property.getProperty(ConstantConnection.USER_KEY);
    private static final String PASSWORD = Property.getProperty(ConstantConnection.PASSWORD_KEY);

    private ConnectionManager() {
    }

    public static Connection getConnection() {
        if (CONNECTION == null) {
            try {
                CONNECTION = DriverManager.getConnection(URL, USER, PASSWORD);
            } catch (SQLException e) {
                LOG.error("SQLException: " + e.getMessage());
                LOG.error("SQLState: " + e.getSQLState());
                LOG.error("VendorError: " + e.getErrorCode());
            }
        }
        return CONNECTION;
    }
}
