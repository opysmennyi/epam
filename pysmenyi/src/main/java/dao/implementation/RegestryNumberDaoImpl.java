package dao.implementation;

import dao.constans_dao.ConstantDAO;
import dao.interfaces.RegestryNumberDAO;
import model.entitydata.RegestryNumber;
import transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RegestryNumberDaoImpl implements RegestryNumberDAO {
    @Override
    public List<RegestryNumber> findAll() throws SQLException {
        List<RegestryNumber> regestryNumbers = new ArrayList<>();
        try (Statement statement = ConstantDAO.CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(ConstantDAO.FIND_ALL_REGESTRYNUMBER)) {
                while (resultSet.next()) {
                    regestryNumbers.add((RegestryNumber) new Transformer(RegestryNumber.class).fromResultSetToAgency(resultSet));
                }
            }
        }
        return regestryNumbers;
    }

    @Override
    public RegestryNumber findById(Integer regestry_number) throws SQLException {
        RegestryNumber regestryNumber = null;
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.FIND_BY_ID_REGESTRYNUMBER)) {
            preparedStatement.setInt(1, regestry_number);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    regestryNumber = (RegestryNumber) new Transformer(RegestryNumber.class).fromResultSetToAgency(resultSet);
                    break;
                }
            }
        }
        return regestryNumber;
    }

    @Override
    public int create(RegestryNumber regestryNumber) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.CREATE_REGESTRYNUMBER)) {
            preparedStatement.setString(1, regestryNumber.getRegestry_number());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(RegestryNumber regestryNumber) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.UPDATE_REGESTRYNUMBER)) {
            preparedStatement.setString(1, regestryNumber.getRegestry_number());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(RegestryNumber id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer regestryNumber) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.DELETE_REGESTRYNUMBER)) {
            preparedStatement.setInt(1, regestryNumber);
            return preparedStatement.executeUpdate();
        }
    }
}
