package dao.implementation;

import dao.constans_dao.ConstantDAO;
import dao.interfaces.PlaceDAO;
import model.entitydata.Place;
import transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class PlaceDaoImpl implements PlaceDAO {
    @Override
    public List<Place> findAll() throws SQLException {
        List<Place> places = new ArrayList<>();
        try (Statement statement = ConstantDAO.CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(ConstantDAO.FIND_ALL_PLACE)) {
                while (resultSet.next()) {
                    places.add((Place) new Transformer(Place.class).fromResultSetToAgency(resultSet));
                }
            }
        }
        return places;
    }

    @Override
    public Place findById(Integer id_place) throws SQLException {
        Place place = null;
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.FIND_BY_ID_PLACE)) {
            preparedStatement.setInt(1, id_place);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    place = (Place) new Transformer(Place.class).fromResultSetToAgency(resultSet);
                    break;
                }
            }
        }
        return place;
    }

    @Override
    public int create(Place place) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.CREATE_PLACE)) {
            preparedStatement.setInt(1, place.getId_place());
            preparedStatement.setInt(2, place.getSquare());
            preparedStatement.setDouble(3, place.getPrice());
            preparedStatement.setString(4, place.getLocation());
            preparedStatement.setString(5, place.getType());
            preparedStatement.setInt(6, place.getLandor_id());
            preparedStatement.setString(7, place.getRegestry_number());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Place place) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.UPDATE_PLACE)) {
            preparedStatement.setInt(1, place.getId_place());
            preparedStatement.setInt(2, place.getSquare());
            preparedStatement.setDouble(3, place.getPrice());
            preparedStatement.setString(4, place.getLocation());
            preparedStatement.setString(5, place.getType());
            preparedStatement.setInt(6, place.getLandor_id());
            preparedStatement.setString(7, place.getRegestry_number());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Place id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_place) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.DELETE_PLACE)) {
            preparedStatement.setInt(1, id_place);
            return preparedStatement.executeUpdate();
        }
    }
}
