package dao.implementation;

import dao.constans_dao.ConstantDAO;
import dao.interfaces.ClientDAO;
import model.entitydata.Client;
import transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClientDaoImpl implements ClientDAO {
    @Override
    public List<Client> findAll() throws SQLException {
        List<Client> clients = new ArrayList<>();
        try (Statement statement = ConstantDAO.CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(ConstantDAO.FIND_ALL_CLIENT)) {
                while (resultSet.next()) {
                    clients.add((Client) new Transformer(Client.class).fromResultSetToAgency(resultSet));
                }
            }
        }
        return clients;
    }

    @Override
    public Client findById(Integer id_client) throws SQLException {
        Client client = null;
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.FIND_BY_ID_CLIENT)) {
            preparedStatement.setInt(1, id_client);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    client = (Client) new Transformer(Client.class).fromResultSetToAgency(resultSet);
                    break;
                }
            }
        }
        return client;
    }

    @Override
    public int create(Client client) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.CREATE_CLIENT)) {
            preparedStatement.setInt(1, client.getId_client());
            preparedStatement.setInt(2, client.getId_agent());
            preparedStatement.setString(3, client.getPersonal_info_passport_id());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Client client) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.UPDATE_CLIENT)) {
            preparedStatement.setInt(1, client.getId_client());
            preparedStatement.setInt(2, client.getId_agent());
            preparedStatement.setString(3, client.getPersonal_info_passport_id());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Client id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_client) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.DELETE_CLIENT)) {
            preparedStatement.setInt(1, id_client);
            return preparedStatement.executeUpdate();
        }
    }
}
