package dao.constans_dao;

import persistant.persistant_implementation.ConnectionManager;

import java.sql.Connection;

public class ConstantDAO {

    public static Connection CONNECTION = ConnectionManager.getConnection();
    public static final String FIND_ALL_AGENCIES = "SELECT * FROM agency";
    public static final String DELETE_AGENCY = "DELETE FROM agency WHERE id_agency = ?";
    public static final String CREATE_AGENCY = "INSERT agency (id_agency, name_agency, city, payment_terms, markup, agency_services_price, agreement_termination) VALUES (?, ?, ?, ?, ?, ?, ?)";
    public static final String UPDATE_AGENCY = "UPDATE agency SET id_agency=?,name_agency=?, city=?,payment_terms=?, markup=?, agency_services_price=?, agreement_termination=? WHERE id_agency=?";
    public static final String FIND_BY_ID_AGENCY = "SELECT * WHERE id_agency=?";

    public static final String FIND_ALL_AGENT = "SELECT * FROM agent";
    public static final String DELETE_AGENT = "DELETE FROM agency WHERE agent_id_agent = ?";
    public static final String CREATE_AGENT = "INSERT agent (agent_id_agent, personal_info_passport_id) VALUES (?, ?)";
    public static final String UPDATE_AGENT = "UPDATE agent SET id_agent=?, personal_info_passport_id=?";
    public static final String FIND_BY_ID_AGENT = "SELECT * FROM agent WHERE id_agent=?";

    public static final String FIND_ALL_AHAD = "SELECT * FROM agent_has_agency";
    public static final String DELETE_AHAD = "DELETE FROM agent_has_agency WHERE agent_id_agent=?";
    public static final String CREATE_AHAD = "INSERT agent ( agent_id_agent, agency_id_agency VALUES (?, ?)";
    public static final String UPDATE_AHAD = "UPDATE  agent_id_agent=? WHERE agency_id_agency=?";
    public static final String FIND_BY_ID_AHAD = "SELECT * FROM agent_has_agency WHERE id_agent=?";

    public static final String FIND_ALL_AHL = "SELECT * FROM agent_has_landlord";
    public static final String DELETE_AHL = "DELETE FROM agent_has_landlord WHERE agent_id_agent=?";
    public static final String CREATE_AHL = "INSERT agent_has_landlord (agent_id_agent, landlord_id_landor) VALUES (?, ?)";
    public static final String UPDATE_AHL = "UPDATE agent_has_landlord SET agent_id_agent=?, landlord_id_landor=?";
    public static final String FIND_BY_ID_AHL = "SELECT * FROM agent_has_landlord WHERE id_agent=?";

    public static final String FIND_ALL_CLIENT = "SELECT * FROM client";
    public static final String DELETE_CLIENT = "DELETE FROM client WHERE id_client=?";
    public static final String CREATE_CLIENT = "INSERT client (id_client, agent_id_agent, personal_info_passport_id) VALUES (?, ?, ?)";
    public static final String UPDATE_CLIENT = "UPDATE client id_client=?, agent_id_agent=?, personal_info_passport_id=?";
    public static final String FIND_BY_ID_CLIENT = "SELECT * FROM client WHERE id_client=?";

    public static final String FIND_ALL_LANDLORD = "SELECT * FROM landlord";
    public static final String DELETE_LANDLORD = "DELETE FROM landlord WHERE id_landor=?";
    public static final String CREATE_LANDLORD = "INSERT landlord (id_landor, personal_info_passport_id) VALUES (?, ?)";
    public static final String UPDATE_LANDLORD = "UPDATE landlord SET id_landor=?, personal_info_passport_id=?";
    public static final String FIND_BY_ID_LANDLORD = "SELECT * FROM landlord WHERE id_landor=?";

    public static final String FIND_ALL_PERSONALINFO = "SELECT * FROM personal_info";
    public static final String DELETE_PERSONALINFO = "DELETE FROM personal_info WHERE passport_id=?";
    public static final String CREATE_PERSONALINFO = "INSERT personal_info ( passport_id, name, surname, age, family_status) VALUES (?, ?, ?, ?, ?)";
    public static final String UPDATE_PERSONALINFO = "UPDATE personal_info SET passport_id=?, name=?, surname=?, age=?, family_status=?";
    public static final String FIND_BY_ID_PERSONALINFO = "SELECT * FROM personal_info WHERE passport_id=?";

    public static final String FIND_ALL_PLACE = "SELECT * FROM place";
    public static final String DELETE_PLACE = "DELETE FROM place WHERE id_place=?";
    public static final String CREATE_PLACE = "INSERT place (id_place, square, price, location, type, landlord_id_landor) VALUES (?, ?, ?, ?, ?, ?)";

    public static final String UPDATE_PLACE = "UPDATE place SET place=?, id_place=?, square=?, price=?, location=?, type=?, landlord_id_landor=?";
    public static final String FIND_BY_ID_PLACE = "SELECT * FROM place WHERE id_place=?";

    public static final String FIND_ALL_REGESTRYNUMBER = "SELECT * FROM regestry_number";
    public static final String DELETE_REGESTRYNUMBER = "DELETE FROM regestry_number WHERE regestry_number=?";
    public static final String CREATE_REGESTRYNUMBER = "INSERT regestry_number (regestry_number) VALUES (?)";
    public static final String UPDATE_REGESTRYNUMBER = "UPDATE regestry_number SET regestry_number=?, regestry_number=?";
    public static final String FIND_BY_ID_REGESTRYNUMBER = "SELECT * FROM regestry_number WHERE regestry_number=?";
}
