package dao.interfaces;

import model.entitydata.Personal_info;

import java.sql.SQLException;

public interface PersonalInfoDAO extends GeneralDAO <Personal_info, Integer> {
    int delete(Integer id) throws SQLException;
}
