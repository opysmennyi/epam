package dao.interfaces;

import model.entitydata.Landlord;

import java.sql.SQLException;

public interface LandlordDAO extends GeneralDAO <Landlord, Integer>{
    int delete(Integer id) throws SQLException;
}
